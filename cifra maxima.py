def ex_0068(x):
    if x >= 2000000000:
        print("EROARE")
    else:
        cifra_maxima = 0
        while x != 0:
            if cifra_maxima < x % 10:
                cifra_maxima = x % 10
            x = x // 10
        print(cifra_maxima)


if __name__ == "__main__":
    x = int(input())
    ex_0068(x)
